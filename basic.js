const assert = require('assert');
const randomEmail = require('random-email');
const path = require('path');

describe('webdriver.io page', () => {
        
    it('pop bubbles in 5 sec', () => {
        
        browser.url('https://task1-bvckdxdkxw.now.sh/');
        
        browser.pause(5000);
        const bubbles = $$('div.bubble');
        const bubblesNumber = bubbles.length;
        
        for (let i = 0; i < bubblesNumber; i++) {
            bubbles[i].click();
        }
        
        const poppedScore = $('div#score').getHTML(false);
 
        assert.equal(bubblesNumber, poppedScore);
        
    });
 
});